
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto(): _raiz(nullptr) , _cardinal(0) {
    // Completar
}

template <class T>
Conjunto<T>::~Conjunto() { 
    // Completar
    destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo *node_itr = _raiz;
    while(node_itr!=nullptr && node_itr->valor != clave)
        if(node_itr->valor<clave)
            node_itr = node_itr->der;
        else
            node_itr = node_itr->izq;
    return (node_itr!=nullptr);
}


template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)){
        Nodo* node_parent = nullptr;
        Nodo* node_itr = _raiz;
        while(node_itr!=nullptr){
            node_parent = node_itr;
            if(node_itr->valor<clave)
                node_itr = node_itr->der;
            else
                node_itr = node_itr->izq;
        }
        Nodo *node = new Nodo(clave,node_parent);
        _cardinal++;
        if(node_parent == nullptr) //caso inicial
            _raiz = node;
        else

        if (node_parent->valor < clave)
            node_parent->der = node;
        else
            node_parent->izq = node;
    }

}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo *node_itr = _raiz;
    while (node_itr->valor != clave){               //Busco el nodo a remover
        if (node_itr->valor < clave)
            node_itr = node_itr->der;
        else
            node_itr = node_itr->izq;
     }

    if (node_itr->izq == nullptr)                    //Si tiene solo un hijo derecho
        Transplant(node_itr,node_itr->der);     // Lo tansplanto
    else if (node_itr->der== nullptr)
        Transplant(node_itr,node_itr->izq);     // Si solo tiene un hijo Izq
                                                     // Lo Transplanto
        else{
        Nodo *node_aux = node_itr->der;
        while(node_aux->izq!= nullptr)               //node_aux <- inmediato Sucesor
            node_aux = node_aux->izq;

        if (node_aux->padre->valor != clave){        // Caso en el que el inmediato sucesor
                                                     //no es el derecho del nodo a  remover
            Transplant(node_aux,node_aux->der); // transplanto el inmediato sucesor por sus hijos  derechos
            node_aux->der = node_itr->der;
            node_aux->der->padre=node_aux;
        }
        Transplant(node_itr,node_aux);
        node_aux->izq = node_itr->izq;
        node_aux->izq->padre = node_aux;
    }
    delete node_itr;
    _cardinal--;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo *node_itr = _raiz;
    while(node_itr->valor!=clave){
        if(node_itr->valor<clave)
            node_itr = node_itr->der;
        else
            node_itr = node_itr->izq;
    }
    if(node_itr->der != nullptr){
        node_itr = node_itr->der;

        while(node_itr->izq!= nullptr)
            node_itr = node_itr->izq;

        return(node_itr->valor);
    }
    else {
        Nodo *node_aux = node_itr->padre;
        while(node_aux != nullptr && node_itr->valor ==node_aux->der->valor ){
            node_itr = node_aux;
            node_aux = node_aux->padre;

        }
        return (node_aux->valor);
    }


}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo *node_itr = _raiz;
    while(node_itr->izq!= nullptr)
        node_itr = node_itr->izq;
    return(node_itr->valor);
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo *node_parent = nullptr;
    Nodo *node_itr = _raiz;
    while(node_itr!= nullptr){
        node_parent = node_itr;
        node_itr = node_itr->der;
    }
    return(node_parent->valor);
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return this->_cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream& os) {
   mostrar_(_raiz,0,os);

}

template<class T>
void Conjunto<T>::Transplant(Conjunto::Nodo *&u, Conjunto::Nodo *&v) {
    if (u->padre == nullptr)
        this->_raiz = v;        //Si quiero transplantar la raiz, directamente
                                //pone el nodo nuevo en la raiz
    else if(u->padre->izq!= nullptr && u->valor == u->padre->izq->valor)
            u->padre->izq = v;
                                //Se fija si el nodo a transplantar
                                //es el hijo izquierdo del padre y lo cambia
        else u->padre->der = v;
                                //Si no cambia el hijo derecho del padre
        if (v!= nullptr)
            v->padre = u->padre;
                                //si no estoy reemplazando por el vacio,
                                //cambia el padre del nuevo nodo
}
template<class T>
void Conjunto<T>::mostrar_(Conjunto::Nodo *&r, int space,std::ostream& os)  {
    if (r!= nullptr){
        mostrar_(r->izq,space+4,os);
        mostrar_(r->der,space+4,os);
        if (space)
            os << std::setw(space) << ' ';

        os<<r->valor<<'\n';
    }
}

template<class T>
void Conjunto<T>::destruir(Conjunto::Nodo *n) {
    if(n!= nullptr){
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }

}
